FROM    alpine:3.10 as builder

ARG     KUBECTL_VERSION
ARG     HELM_VERSION

RUN     wget https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
          && chmod +x ./kubectl \
          && wget https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz \
          && tar -zxvf helm-${HELM_VERSION}-linux-amd64.tar.gz

##################################################

FROM    gcr.io/distroless/base

COPY    --from=builder   /kubectl            /usr/local/bin/kubectl
COPY    --from=builder   /linux-amd64/helm   /usr/local/bin/helm
